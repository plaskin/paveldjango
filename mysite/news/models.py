from django.db import models
from django.contrib.auth.models import User
from django.db.models.deletion import CASCADE

class Author(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)

class Category(models.Model):
    category = models.CharField(max_length=20)

class Post(models.Model):
    author = models.ForeignKey(Author, on_delete=CASCADE)

class postCategory(models.Model):
    pass

class Comment(models.Model):
    pass